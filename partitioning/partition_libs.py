'''
Created on 23 Sep 2018

@author: david

Dependencies:
- pymysql
'''
import ctypes
import logging
import os
import platform

try:
  import pymysql
except ImportError:
  pass

from abc import abstractmethod

logger = logging.getLogger(__name__)

def get_free_space(path = '.'):
    """
    Return folder/drive free space (in megabytes).
    
    https://stackoverflow.com/questions/51658/cross-platform-space-remaining-on-volume-using-python#
    """
    if platform.system() == 'Windows':
        free_bytes = ctypes.c_ulonglong(0)
        ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(path), None, None, ctypes.pointer(free_bytes))
        return free_bytes.value / 1048576 # 1024 * 1024
    else:
        st = os.statvfs(path)
        return st.f_bavail * st.f_frsize / 1048576 # 1024 * 1024
      
  
class database(object):
  
  partition_tables = {
    'history':      'D',
    'history_uint': 'D',
    'history_str':  'D',
    'history_text': 'D',
    'history_log':  'D',
    'trends':       'M',
    'trends_uint':  'M',
  }
  
  
  @abstractmethod
  def fetch_data_dir(self, schema_name = None):
    '''
    Determine directory where database stores data.
    '''
    pass
  
  @abstractmethod
  def table_partition_list(self, table_name):
    pass
  
  @abstractmethod
  def table_partition_add(self, table_name, upper_date_limit):
    pass
  
  @abstractmethod
  def table_partition_del(self, table_name, partition_name):
    pass
  
  @abstractmethod
  def table_partition_optimize(self, table_name, partition_name):
    pass
  
class mysql(database):
  '''
  
  '''
    
  def __init__(self, hostname, username, password, schema, schema_archive = 'zabbix_archive'):
    self._db = pymysql.connect(
      host=hostname, user=username, password=password, db=schema, 
      charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor,
      autocommit=False,
    )
    self._schema_active = schema
    self._schema_archive = schema_archive
    
  def fetch_variable_value(self, var_name):
    
    try:
      with self._db.cursor() as cursor:
        sql = 'SELECT `Variable_Value` FROM `information_schema`.`GLOBAL_VARIABLES` WHERE `Variable_Name` = %s'
        cursor.execute(sql, (var_name,))
        result = cursor.fetchone()
        logger.debug("SQL Result: {0}".format(result))
        return result['Variable_Value']
    except Exception as e:
      logger.error("Error: {0}".format(e))
      return None
  
  def fetch_data_dir(self, schema_name = None):
    '''
    
    '''
    return self.fetch_variable_value('datadir')
  
  def table_partition_list(self, table_name):
    '''
    Fetch the current partitions for the specified table.
    
    Returns:
    
    '''    
    try:
      with self._db.cursor() as cursor:
        sql = '''
          SELECT 
            *,
            FROM_UNIXTIME(`PARTITION_DESCRIPTION`) AS `PARTITION_END_DATE`,
            (`DATA_LENGTH` + `INDEX_LENGTH` + `DATA_FREE`) AS `TOTAL_SIZE`
          FROM
            `information_schema`.`PARTITIONS`
          WHERE
            `TABLE_SCHEMA` = %s
            AND
            `TABLE_NAME` = %s
            AND
            `PARTITION_METHOD` IS NOT NULL
          ORDER BY
            `PARTITION_DESCRIPTION` ASC
        '''
        cursor.execute(sql, (self._schema_active, table_name,))
        result = cursor.fetchall()
        # logger.debug("SQL Result: {0}".format(result))      
        return result
    except Exception as e:
      logger.error("Error: {0}".format(e))
      return None
      
  def table_partition_add(self, table_name, upper_date_limit):
    '''
    
    e.g.
    ALTER TABLE `history_uint` ADD PARTITION (
      PARTITION p20110523 VALUES LESS THAN (UNIX_TIMESTAMP("2011-05-24 00:00:00"))
    );
    '''
    try:
      with self._db.cursor() as cursor:
        # Ensure we are working with a datetime
        upper_date_limit.replace(hour=0, minute=0, second=0, microsecond=0)
        partition_name = upper_date_limit.strftime("p%Y%m%d")
        partition_condition = upper_date_limit.strftime("%Y-%m-%d %H:%M:%S")
        cursor.execute(
          '''
          ALTER TABLE %s ADD PARTITION (
            PARTITION %s VALUES LESS THAN (UNIX_TIMESTAMP(%s))
          );
          ''', (table_name, partition_name, partition_condition, ))
      self._db.commit()
    except Exception as e:
      logger.error("Error: {0}".format(e))
      return None
  
  
  def table_partition_del(self, table_name, partition_name):
    '''
    ALTER TABLE `history_uint` DROP PARTITION p20110515;
    '''
    pass
  
  def table_partition_optimize(self, table_name, partition_name):
    '''
    
    ALTER TABLE t1 REBUILD PARTITION p0;
    '''
    pass
    
    
class postgresql(database):
  
  def __init__(self, hostname, username, password, schema):
    
    pass