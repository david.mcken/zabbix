#!/usr/bin/python3
'''
Created on 12 Sep 2018

@author: david
'''
import datetime
import logging
import os
import pprint
import sys

# Add root of zabbix scripts to allow the package imports to succeed
sys.path.insert(0, os.path.abspath('..'))

import partitioning.partition_libs
from config import config


# Date range generator
def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + datetime.timedelta(n)

def generate_partitions():
  start = datetime.date.today()
  end = datetime.date.today() + datetime.timedelta(days = 45)
  
  print("PARTITION BY RANGE (`clock`)\n(")
  for curr_day in daterange(start, end):
    next_day = curr_day + datetime.timedelta(days = 1)
    print('  PARTITION p{0}_{1:02}_{2:02} VALUES LESS THAN (UNIX_TIMESTAMP("{3}-{4:02}-{5:02} 00:00:00")),'.\
          format(curr_day.year, curr_day.month, curr_day.day, next_day.year, next_day.month, next_day.day))
  
  print(");")

if __name__ == '__main__':
  logging.BASIC_FORMAT = '%(asctime)s - %(name)s - %(thread)d - %(levelname)s - %(message)s'
  logging.getLogger('requests.packages.urllib3.connectionpool').setLevel(logging.ERROR)
  logging.getLogger('partition_libs').setLevel(logging.ERROR)
  logging.basicConfig(level = logging.DEBUG)

  #print(os.getcwd())
  #print(partition_libs.get_free_space())
  db = partitioning.partition_libs.mysql(
    hostname = config['db_hostname'],
    username = config['db_username'],
    password = config['db_password'],
    schema = config['db_schema'],
  )

  data_dir = db.fetch_data_dir()
  data_dir_free = partitioning.partition_libs.get_free_space(data_dir)
  
  logging.info("Data directory: {0} - {1}".format(data_dir, data_dir_free))
  
  # generate_partitions()
  # ALTER TABLE `history_uint` ADD PARTITION (  PARTITION `p2018_12_10` VALUES LESS THAN (UNIX_TIMESTAMP("2018-12-11 00:00:00")) );

  end_date = datetime.datetime.now() + datetime.timedelta(days = 60)
  partitions = db.table_partition_list('history_uint')
  
  biggest_partition = None
  for curr_partition in partitions:
    if biggest_partition == None or biggest_partition < curr_partition['PARTITION_END_DATE']:
      biggest_partition = curr_partition['PARTITION_END_DATE']
      
  # logging.error(pprint.pformat(partitions))
  logging.error("Largest existing partition: {0}".format(biggest_partition))
  
  partitions_to_create = 0
  for curr_day in daterange(biggest_partition, end_date):
    next_day = curr_day + datetime.timedelta(days = 1)
    print('ALTER TABLE `zabbix`.`history_uint` ADD PARTITION (PARTITION `p{0}_{1:02}_{2:02}` VALUES LESS THAN (UNIX_TIMESTAMP("{3}-{4:02}-{5:02} 00:00:00")));'.\
                  format(curr_day.year, curr_day.month, curr_day.day, next_day.year, next_day.month, next_day.day))
    partitions_to_create += 1
    
  logging.error("Partitions to create: {0}".format(partitions_to_create))
  
  
