'''
Example credential config file.

Copy to config.py and fill in as appropriate.
'''

config = {
  'db_hostname': '127.0.0.1',
  'db_username': 'root',
  'db_password': 'rootpassword',
  'db_schema': 'zabbix',
}
